var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1900",
        "ok": "1900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7737",
        "ok": "7737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "752",
        "ok": "752",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles3": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1845,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.393",
        "ok": "7.393",
        "ko": "-"
    }
},
contents: {
"req_first-unauthent-b0694": {
        type: "REQUEST",
        name: "First unauthenticated request",
path: "First unauthenticated request",
pathFormatted: "req_first-unauthent-b0694",
stats: {
    "name": "First unauthenticated request",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles2": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "percentiles3": {
        "total": "136",
        "ok": "136",
        "ko": "-"
    },
    "percentiles4": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.389",
        "ok": "0.389",
        "ko": "-"
    }
}
    },"req_authentication-c75f7": {
        type: "REQUEST",
        name: "Authentication",
path: "Authentication",
pathFormatted: "req_authentication-c75f7",
stats: {
    "name": "Authentication",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6924",
        "ok": "6924",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "313",
        "ok": "313",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "903",
        "ok": "903",
        "ko": "-"
    },
    "percentiles1": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "127",
        "ok": "127",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1221",
        "ok": "1221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 94,
    "percentage": 94
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 5
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.389",
        "ok": "0.389",
        "ko": "-"
    }
}
    },"req_authenticated-r-6889d": {
        type: "REQUEST",
        name: "Authenticated request",
path: "Authenticated request",
pathFormatted: "req_authenticated-r-6889d",
stats: {
    "name": "Authenticated request",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7038",
        "ok": "7038",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "171",
        "ok": "171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "896",
        "ok": "896",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14",
        "ok": "14",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles3": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5706",
        "ok": "5706",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 97,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.389",
        "ok": "0.389",
        "ko": "-"
    }
}
    },"req_get-all-countri-59c86": {
        type: "REQUEST",
        name: "Get all countries",
path: "Get all countries",
pathFormatted: "req_get-all-countri-59c86",
stats: {
    "name": "Get all countries",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6975",
        "ok": "6975",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "134",
        "ok": "134",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "648",
        "ok": "648",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "percentiles3": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3550",
        "ok": "3550",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 195,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.778",
        "ok": "0.778",
        "ko": "-"
    }
}
    },"req_create-new-coun-1d907": {
        type: "REQUEST",
        name: "Create new country",
path: "Create new country",
pathFormatted: "req_create-new-coun-1d907",
stats: {
    "name": "Create new country",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7301",
        "ok": "7301",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "575",
        "ok": "575",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles3": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "906",
        "ok": "906",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 196,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.778",
        "ok": "0.778",
        "ko": "-"
    }
}
    },"req_get-created-cou-6beba": {
        type: "REQUEST",
        name: "Get created country",
path: "Get created country",
pathFormatted: "req_get-created-cou-6beba",
stats: {
    "name": "Get created country",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7737",
        "ok": "7737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "195",
        "ok": "195",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "868",
        "ok": "868",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles3": {
        "total": "387",
        "ok": "387",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3915",
        "ok": "3915",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 963,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 36,
    "percentage": 4
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.891",
        "ok": "3.891",
        "ko": "-"
    }
}
    },"req_delete-created--f9574": {
        type: "REQUEST",
        name: "Delete created country",
path: "Delete created country",
pathFormatted: "req_delete-created--f9574",
stats: {
    "name": "Delete created country",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "297",
        "ok": "297",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles3": {
        "total": "123",
        "ok": "123",
        "ko": "-"
    },
    "percentiles4": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 200,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.778",
        "ok": "0.778",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
