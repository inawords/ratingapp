package at.fhv.msc.sem1.ratingapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class CinemaMapperTest {

    private CinemaMapper cinemaMapper;

    @BeforeEach
    public void setUp() {
        cinemaMapper = new CinemaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(cinemaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(cinemaMapper.fromId(null)).isNull();
    }
}
