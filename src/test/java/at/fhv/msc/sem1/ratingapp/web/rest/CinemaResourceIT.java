package at.fhv.msc.sem1.ratingapp.web.rest;

import at.fhv.msc.sem1.ratingapp.RatingApp;
import at.fhv.msc.sem1.ratingapp.domain.Cinema;
import at.fhv.msc.sem1.ratingapp.repository.CinemaRepository;
import at.fhv.msc.sem1.ratingapp.service.CinemaService;
import at.fhv.msc.sem1.ratingapp.service.dto.CinemaDTO;
import at.fhv.msc.sem1.ratingapp.service.mapper.CinemaMapper;
import at.fhv.msc.sem1.ratingapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static at.fhv.msc.sem1.ratingapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CinemaResource} REST controller.
 */
@SpringBootTest(classes = RatingApp.class)
public class CinemaResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_ROOMS = 1;
    private static final Integer UPDATED_ROOMS = 2;

    @Autowired
    private CinemaRepository cinemaRepository;

    @Autowired
    private CinemaMapper cinemaMapper;

    @Autowired
    private CinemaService cinemaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCinemaMockMvc;

    private Cinema cinema;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CinemaResource cinemaResource = new CinemaResource(cinemaService);
        this.restCinemaMockMvc = MockMvcBuilders.standaloneSetup(cinemaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cinema createEntity(EntityManager em) {
        Cinema cinema = new Cinema()
            .name(DEFAULT_NAME)
            .rooms(DEFAULT_ROOMS);
        return cinema;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cinema createUpdatedEntity(EntityManager em) {
        Cinema cinema = new Cinema()
            .name(UPDATED_NAME)
            .rooms(UPDATED_ROOMS);
        return cinema;
    }

    @BeforeEach
    public void initTest() {
        cinema = createEntity(em);
    }

    @Test
    @Transactional
    public void createCinema() throws Exception {
        int databaseSizeBeforeCreate = cinemaRepository.findAll().size();

        // Create the Cinema
        CinemaDTO cinemaDTO = cinemaMapper.toDto(cinema);
        restCinemaMockMvc.perform(post("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isCreated());

        // Validate the Cinema in the database
        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeCreate + 1);
        Cinema testCinema = cinemaList.get(cinemaList.size() - 1);
        assertThat(testCinema.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCinema.getRooms()).isEqualTo(DEFAULT_ROOMS);
    }

    @Test
    @Transactional
    public void createCinemaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cinemaRepository.findAll().size();

        // Create the Cinema with an existing ID
        cinema.setId(1L);
        CinemaDTO cinemaDTO = cinemaMapper.toDto(cinema);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCinemaMockMvc.perform(post("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cinema in the database
        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = cinemaRepository.findAll().size();
        // set the field null
        cinema.setName(null);

        // Create the Cinema, which fails.
        CinemaDTO cinemaDTO = cinemaMapper.toDto(cinema);

        restCinemaMockMvc.perform(post("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isBadRequest());

        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRoomsIsRequired() throws Exception {
        int databaseSizeBeforeTest = cinemaRepository.findAll().size();
        // set the field null
        cinema.setRooms(null);

        // Create the Cinema, which fails.
        CinemaDTO cinemaDTO = cinemaMapper.toDto(cinema);

        restCinemaMockMvc.perform(post("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isBadRequest());

        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCinemas() throws Exception {
        // Initialize the database
        cinemaRepository.saveAndFlush(cinema);

        // Get all the cinemaList
        restCinemaMockMvc.perform(get("/api/cinemas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cinema.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].rooms").value(hasItem(DEFAULT_ROOMS)));
    }
    
    @Test
    @Transactional
    public void getCinema() throws Exception {
        // Initialize the database
        cinemaRepository.saveAndFlush(cinema);

        // Get the cinema
        restCinemaMockMvc.perform(get("/api/cinemas/{id}", cinema.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cinema.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.rooms").value(DEFAULT_ROOMS));
    }

    @Test
    @Transactional
    public void getNonExistingCinema() throws Exception {
        // Get the cinema
        restCinemaMockMvc.perform(get("/api/cinemas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCinema() throws Exception {
        // Initialize the database
        cinemaRepository.saveAndFlush(cinema);

        int databaseSizeBeforeUpdate = cinemaRepository.findAll().size();

        // Update the cinema
        Cinema updatedCinema = cinemaRepository.findById(cinema.getId()).get();
        // Disconnect from session so that the updates on updatedCinema are not directly saved in db
        em.detach(updatedCinema);
        updatedCinema
            .name(UPDATED_NAME)
            .rooms(UPDATED_ROOMS);
        CinemaDTO cinemaDTO = cinemaMapper.toDto(updatedCinema);

        restCinemaMockMvc.perform(put("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isOk());

        // Validate the Cinema in the database
        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeUpdate);
        Cinema testCinema = cinemaList.get(cinemaList.size() - 1);
        assertThat(testCinema.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCinema.getRooms()).isEqualTo(UPDATED_ROOMS);
    }

    @Test
    @Transactional
    public void updateNonExistingCinema() throws Exception {
        int databaseSizeBeforeUpdate = cinemaRepository.findAll().size();

        // Create the Cinema
        CinemaDTO cinemaDTO = cinemaMapper.toDto(cinema);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCinemaMockMvc.perform(put("/api/cinemas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cinemaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cinema in the database
        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCinema() throws Exception {
        // Initialize the database
        cinemaRepository.saveAndFlush(cinema);

        int databaseSizeBeforeDelete = cinemaRepository.findAll().size();

        // Delete the cinema
        restCinemaMockMvc.perform(delete("/api/cinemas/{id}", cinema.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cinema> cinemaList = cinemaRepository.findAll();
        assertThat(cinemaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
