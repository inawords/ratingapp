package at.fhv.msc.sem1.ratingapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import at.fhv.msc.sem1.ratingapp.web.rest.TestUtil;

public class CinemaDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CinemaDTO.class);
        CinemaDTO cinemaDTO1 = new CinemaDTO();
        cinemaDTO1.setId(1L);
        CinemaDTO cinemaDTO2 = new CinemaDTO();
        assertThat(cinemaDTO1).isNotEqualTo(cinemaDTO2);
        cinemaDTO2.setId(cinemaDTO1.getId());
        assertThat(cinemaDTO1).isEqualTo(cinemaDTO2);
        cinemaDTO2.setId(2L);
        assertThat(cinemaDTO1).isNotEqualTo(cinemaDTO2);
        cinemaDTO1.setId(null);
        assertThat(cinemaDTO1).isNotEqualTo(cinemaDTO2);
    }
}
