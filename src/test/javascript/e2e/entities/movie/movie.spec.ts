import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import MovieComponentsPage, { MovieDeleteDialog } from './movie.page-object';
import MovieUpdatePage from './movie-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Movie e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let movieComponentsPage: MovieComponentsPage;
  let movieUpdatePage: MovieUpdatePage;
  let movieDeleteDialog: MovieDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Movies', async () => {
    await navBarPage.getEntityPage('movie');
    movieComponentsPage = new MovieComponentsPage();
    expect(await movieComponentsPage.getTitle().getText()).to.match(/Movies/);
  });

  it('should load create Movie page', async () => {
    await movieComponentsPage.clickOnCreateButton();
    movieUpdatePage = new MovieUpdatePage();
    expect(await movieUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Movie/);
    await movieUpdatePage.cancel();
  });

  it('should create and save Movies', async () => {
    async function createMovie() {
      await movieComponentsPage.clickOnCreateButton();
      await movieUpdatePage.setTitleInput('title');
      expect(await movieUpdatePage.getTitleInput()).to.match(/title/);
      await movieUpdatePage.setReleaseInput('5');
      expect(await movieUpdatePage.getReleaseInput()).to.eq('5');
      await movieUpdatePage.setDurationInput('5');
      expect(await movieUpdatePage.getDurationInput()).to.eq('5');
      // movieUpdatePage.genreSelectLastOption();
      // movieUpdatePage.actorSelectLastOption();
      // movieUpdatePage.directorSelectLastOption();
      await movieUpdatePage.countrySelectLastOption();
      await waitUntilDisplayed(movieUpdatePage.getSaveButton());
      await movieUpdatePage.save();
      await waitUntilHidden(movieUpdatePage.getSaveButton());
      expect(await movieUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createMovie();
    await movieComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await movieComponentsPage.countDeleteButtons();
    await createMovie();
    await movieComponentsPage.waitUntilLoaded();

    await movieComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await movieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Movie', async () => {
    await movieComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await movieComponentsPage.countDeleteButtons();
    await movieComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    movieDeleteDialog = new MovieDeleteDialog();
    expect(await movieDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/ratingApp.movie.delete.question/);
    await movieDeleteDialog.clickOnConfirmButton();

    await movieComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await movieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
