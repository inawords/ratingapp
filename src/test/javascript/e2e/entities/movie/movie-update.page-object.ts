import { element, by, ElementFinder } from 'protractor';

export default class MovieUpdatePage {
  pageTitle: ElementFinder = element(by.id('ratingApp.movie.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  titleInput: ElementFinder = element(by.css('input#movie-title'));
  releaseInput: ElementFinder = element(by.css('input#movie-release'));
  durationInput: ElementFinder = element(by.css('input#movie-duration'));
  genreSelect: ElementFinder = element(by.css('select#movie-genre'));
  actorSelect: ElementFinder = element(by.css('select#movie-actor'));
  directorSelect: ElementFinder = element(by.css('select#movie-director'));
  countrySelect: ElementFinder = element(by.css('select#movie-country'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setTitleInput(title) {
    await this.titleInput.sendKeys(title);
  }

  async getTitleInput() {
    return this.titleInput.getAttribute('value');
  }

  async setReleaseInput(release) {
    await this.releaseInput.sendKeys(release);
  }

  async getReleaseInput() {
    return this.releaseInput.getAttribute('value');
  }

  async setDurationInput(duration) {
    await this.durationInput.sendKeys(duration);
  }

  async getDurationInput() {
    return this.durationInput.getAttribute('value');
  }

  async genreSelectLastOption() {
    await this.genreSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async genreSelectOption(option) {
    await this.genreSelect.sendKeys(option);
  }

  getGenreSelect() {
    return this.genreSelect;
  }

  async getGenreSelectedOption() {
    return this.genreSelect.element(by.css('option:checked')).getText();
  }

  async actorSelectLastOption() {
    await this.actorSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async actorSelectOption(option) {
    await this.actorSelect.sendKeys(option);
  }

  getActorSelect() {
    return this.actorSelect;
  }

  async getActorSelectedOption() {
    return this.actorSelect.element(by.css('option:checked')).getText();
  }

  async directorSelectLastOption() {
    await this.directorSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async directorSelectOption(option) {
    await this.directorSelect.sendKeys(option);
  }

  getDirectorSelect() {
    return this.directorSelect;
  }

  async getDirectorSelectedOption() {
    return this.directorSelect.element(by.css('option:checked')).getText();
  }

  async countrySelectLastOption() {
    await this.countrySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async countrySelectOption(option) {
    await this.countrySelect.sendKeys(option);
  }

  getCountrySelect() {
    return this.countrySelect;
  }

  async getCountrySelectedOption() {
    return this.countrySelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
