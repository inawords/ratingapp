import { element, by, ElementFinder } from 'protractor';

export default class PersonUpdatePage {
  pageTitle: ElementFinder = element(by.id('ratingApp.person.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  firstNameInput: ElementFinder = element(by.css('input#person-firstName'));
  lastNameInput: ElementFinder = element(by.css('input#person-lastName'));
  isActorInput: ElementFinder = element(by.css('input#person-isActor'));
  isDirectorInput: ElementFinder = element(by.css('input#person-isDirector'));
  countrySelect: ElementFinder = element(by.css('select#person-country'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setFirstNameInput(firstName) {
    await this.firstNameInput.sendKeys(firstName);
  }

  async getFirstNameInput() {
    return this.firstNameInput.getAttribute('value');
  }

  async setLastNameInput(lastName) {
    await this.lastNameInput.sendKeys(lastName);
  }

  async getLastNameInput() {
    return this.lastNameInput.getAttribute('value');
  }

  getIsActorInput() {
    return this.isActorInput;
  }
  getIsDirectorInput() {
    return this.isDirectorInput;
  }
  async countrySelectLastOption() {
    await this.countrySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async countrySelectOption(option) {
    await this.countrySelect.sendKeys(option);
  }

  getCountrySelect() {
    return this.countrySelect;
  }

  async getCountrySelectedOption() {
    return this.countrySelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
