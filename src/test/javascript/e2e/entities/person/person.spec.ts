import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import PersonComponentsPage, { PersonDeleteDialog } from './person.page-object';
import PersonUpdatePage from './person-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Person e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let personComponentsPage: PersonComponentsPage;
  let personUpdatePage: PersonUpdatePage;
  let personDeleteDialog: PersonDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load People', async () => {
    await navBarPage.getEntityPage('person');
    personComponentsPage = new PersonComponentsPage();
    expect(await personComponentsPage.getTitle().getText()).to.match(/People/);
  });

  it('should load create Person page', async () => {
    await personComponentsPage.clickOnCreateButton();
    personUpdatePage = new PersonUpdatePage();
    expect(await personUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Person/);
    await personUpdatePage.cancel();
  });

  it('should create and save People', async () => {
    async function createPerson() {
      await personComponentsPage.clickOnCreateButton();
      await personUpdatePage.setFirstNameInput('firstName');
      expect(await personUpdatePage.getFirstNameInput()).to.match(/firstName/);
      await personUpdatePage.setLastNameInput('lastName');
      expect(await personUpdatePage.getLastNameInput()).to.match(/lastName/);
      const selectedIsActor = await personUpdatePage.getIsActorInput().isSelected();
      if (selectedIsActor) {
        await personUpdatePage.getIsActorInput().click();
        expect(await personUpdatePage.getIsActorInput().isSelected()).to.be.false;
      } else {
        await personUpdatePage.getIsActorInput().click();
        expect(await personUpdatePage.getIsActorInput().isSelected()).to.be.true;
      }
      const selectedIsDirector = await personUpdatePage.getIsDirectorInput().isSelected();
      if (selectedIsDirector) {
        await personUpdatePage.getIsDirectorInput().click();
        expect(await personUpdatePage.getIsDirectorInput().isSelected()).to.be.false;
      } else {
        await personUpdatePage.getIsDirectorInput().click();
        expect(await personUpdatePage.getIsDirectorInput().isSelected()).to.be.true;
      }
      await personUpdatePage.countrySelectLastOption();
      await waitUntilDisplayed(personUpdatePage.getSaveButton());
      await personUpdatePage.save();
      await waitUntilHidden(personUpdatePage.getSaveButton());
      expect(await personUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createPerson();
    await personComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await personComponentsPage.countDeleteButtons();
    await createPerson();
    await personComponentsPage.waitUntilLoaded();

    await personComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await personComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Person', async () => {
    await personComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await personComponentsPage.countDeleteButtons();
    await personComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    personDeleteDialog = new PersonDeleteDialog();
    expect(await personDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/ratingApp.person.delete.question/);
    await personDeleteDialog.clickOnConfirmButton();

    await personComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await personComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
