import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import CinemaComponentsPage, { CinemaDeleteDialog } from './cinema.page-object';
import CinemaUpdatePage from './cinema-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Cinema e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let cinemaComponentsPage: CinemaComponentsPage;
  let cinemaUpdatePage: CinemaUpdatePage;
  let cinemaDeleteDialog: CinemaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Cinemas', async () => {
    await navBarPage.getEntityPage('cinema');
    cinemaComponentsPage = new CinemaComponentsPage();
    expect(await cinemaComponentsPage.getTitle().getText()).to.match(/Cinemas/);
  });

  it('should load create Cinema page', async () => {
    await cinemaComponentsPage.clickOnCreateButton();
    cinemaUpdatePage = new CinemaUpdatePage();
    expect(await cinemaUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Cinema/);
    await cinemaUpdatePage.cancel();
  });

  it('should create and save Cinemas', async () => {
    async function createCinema() {
      await cinemaComponentsPage.clickOnCreateButton();
      await cinemaUpdatePage.setNameInput('name');
      expect(await cinemaUpdatePage.getNameInput()).to.match(/name/);
      await cinemaUpdatePage.setRoomsInput('5');
      expect(await cinemaUpdatePage.getRoomsInput()).to.eq('5');
      await waitUntilDisplayed(cinemaUpdatePage.getSaveButton());
      await cinemaUpdatePage.save();
      await waitUntilHidden(cinemaUpdatePage.getSaveButton());
      expect(await cinemaUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createCinema();
    await cinemaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await cinemaComponentsPage.countDeleteButtons();
    await createCinema();
    await cinemaComponentsPage.waitUntilLoaded();

    await cinemaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await cinemaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Cinema', async () => {
    await cinemaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await cinemaComponentsPage.countDeleteButtons();
    await cinemaComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    cinemaDeleteDialog = new CinemaDeleteDialog();
    expect(await cinemaDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/ratingApp.cinema.delete.question/);
    await cinemaDeleteDialog.clickOnConfirmButton();

    await cinemaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await cinemaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
