import { element, by, ElementFinder } from 'protractor';

export default class CinemaUpdatePage {
  pageTitle: ElementFinder = element(by.id('ratingApp.cinema.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#cinema-name'));
  roomsInput: ElementFinder = element(by.css('input#cinema-rooms'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setRoomsInput(rooms) {
    await this.roomsInput.sendKeys(rooms);
  }

  async getRoomsInput() {
    return this.roomsInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
