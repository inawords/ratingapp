import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import RatingComponentsPage, { RatingDeleteDialog } from './rating.page-object';
import RatingUpdatePage from './rating-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Rating e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let ratingComponentsPage: RatingComponentsPage;
  let ratingUpdatePage: RatingUpdatePage;
  let ratingDeleteDialog: RatingDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Ratings', async () => {
    await navBarPage.getEntityPage('rating');
    ratingComponentsPage = new RatingComponentsPage();
    expect(await ratingComponentsPage.getTitle().getText()).to.match(/Ratings/);
  });

  it('should load create Rating page', async () => {
    await ratingComponentsPage.clickOnCreateButton();
    ratingUpdatePage = new RatingUpdatePage();
    expect(await ratingUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Rating/);
    await ratingUpdatePage.cancel();
  });

  it('should create and save Ratings', async () => {
    async function createRating() {
      await ratingComponentsPage.clickOnCreateButton();
      await ratingUpdatePage.setStarsInput('5');
      expect(await ratingUpdatePage.getStarsInput()).to.eq('5');
      await ratingUpdatePage.setCommentInput('comment');
      expect(await ratingUpdatePage.getCommentInput()).to.match(/comment/);
      await ratingUpdatePage.movieSelectLastOption();
      await waitUntilDisplayed(ratingUpdatePage.getSaveButton());
      await ratingUpdatePage.save();
      await waitUntilHidden(ratingUpdatePage.getSaveButton());
      expect(await ratingUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createRating();
    await ratingComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await ratingComponentsPage.countDeleteButtons();
    await createRating();
    await ratingComponentsPage.waitUntilLoaded();

    await ratingComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await ratingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Rating', async () => {
    await ratingComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await ratingComponentsPage.countDeleteButtons();
    await ratingComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    ratingDeleteDialog = new RatingDeleteDialog();
    expect(await ratingDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/ratingApp.rating.delete.question/);
    await ratingDeleteDialog.clickOnConfirmButton();

    await ratingComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await ratingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
