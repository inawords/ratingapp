import { element, by, ElementFinder } from 'protractor';

export default class RatingUpdatePage {
  pageTitle: ElementFinder = element(by.id('ratingApp.rating.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  starsInput: ElementFinder = element(by.css('input#rating-stars'));
  commentInput: ElementFinder = element(by.css('input#rating-comment'));
  movieSelect: ElementFinder = element(by.css('select#rating-movie'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setStarsInput(stars) {
    await this.starsInput.sendKeys(stars);
  }

  async getStarsInput() {
    return this.starsInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  async movieSelectLastOption() {
    await this.movieSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async movieSelectOption(option) {
    await this.movieSelect.sendKeys(option);
  }

  getMovieSelect() {
    return this.movieSelect;
  }

  async getMovieSelectedOption() {
    return this.movieSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
