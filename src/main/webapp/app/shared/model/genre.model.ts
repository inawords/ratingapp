import { IMovie } from 'app/shared/model/movie.model';

export interface IGenre {
  id?: number;
  name?: string;
  movies?: IMovie[];
}

export const defaultValue: Readonly<IGenre> = {};
