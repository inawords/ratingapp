import { IRating } from 'app/shared/model/rating.model';
import { IGenre } from 'app/shared/model/genre.model';
import { IPerson } from 'app/shared/model/person.model';

export interface IMovie {
  id?: number;
  title?: string;
  release?: number;
  duration?: number;
  ratings?: IRating[];
  genres?: IGenre[];
  actors?: IPerson[];
  directors?: IPerson[];
  countryId?: number;
}

export const defaultValue: Readonly<IMovie> = {};
