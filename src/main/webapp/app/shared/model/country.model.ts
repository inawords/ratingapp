import { IMovie } from 'app/shared/model/movie.model';
import { IPerson } from 'app/shared/model/person.model';

export interface ICountry {
  id?: number;
  name?: string;
  movies?: IMovie[];
  people?: IPerson[];
}

export const defaultValue: Readonly<ICountry> = {};
