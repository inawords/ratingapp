export interface ICinema {
  id?: number;
  name?: string;
  rooms?: number;
}

export const defaultValue: Readonly<ICinema> = {};
