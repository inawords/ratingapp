import { IMovie } from 'app/shared/model/movie.model';

export interface IPerson {
  id?: number;
  firstName?: string;
  lastName?: string;
  isActor?: boolean;
  isDirector?: boolean;
  countryId?: number;
  starrings?: IMovie[];
  productions?: IMovie[];
}

export const defaultValue: Readonly<IPerson> = {
  isActor: false,
  isDirector: false
};
