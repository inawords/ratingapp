export interface IRating {
  id?: number;
  stars?: number;
  comment?: string;
  movieId?: number;
}

export const defaultValue: Readonly<IRating> = {};
