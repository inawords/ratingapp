import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './cinema.reducer';
import { ICinema } from 'app/shared/model/cinema.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICinemaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CinemaDetail = (props: ICinemaDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { cinemaEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Cinema [<b>{cinemaEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{cinemaEntity.name}</dd>
          <dt>
            <span id="rooms">Rooms</span>
          </dt>
          <dd>{cinemaEntity.rooms}</dd>
        </dl>
        <Button tag={Link} to="/cinema" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/cinema/${cinemaEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ cinema }: IRootState) => ({
  cinemaEntity: cinema.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CinemaDetail);
