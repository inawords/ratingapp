import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './cinema.reducer';
import { ICinema } from 'app/shared/model/cinema.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICinemaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Cinema = (props: ICinemaProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { cinemaList, match } = props;
  return (
    <div>
      <h2 id="cinema-heading">
        Cinemas
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Cinema
        </Link>
      </h2>
      <div className="table-responsive">
        {cinemaList && cinemaList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Rooms</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {cinemaList.map((cinema, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${cinema.id}`} color="link" size="sm">
                      {cinema.id}
                    </Button>
                  </td>
                  <td>{cinema.name}</td>
                  <td>{cinema.rooms}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${cinema.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${cinema.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${cinema.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">No Cinemas found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ cinema }: IRootState) => ({
  cinemaList: cinema.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Cinema);
