import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGenre } from 'app/shared/model/genre.model';
import { getEntities as getGenres } from 'app/entities/genre/genre.reducer';
import { IPerson } from 'app/shared/model/person.model';
import { getEntities as getPeople } from 'app/entities/person/person.reducer';
import { ICountry } from 'app/shared/model/country.model';
import { getEntities as getCountries } from 'app/entities/country/country.reducer';
import { getEntity, updateEntity, createEntity, reset } from './movie.reducer';
import { IMovie } from 'app/shared/model/movie.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IMovieUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MovieUpdate = (props: IMovieUpdateProps) => {
  const [idsgenre, setIdsgenre] = useState([]);
  const [idsactor, setIdsactor] = useState([]);
  const [idsdirector, setIdsdirector] = useState([]);
  const [countryId, setCountryId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { movieEntity, genres, people, countries, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/movie');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getGenres();
    props.getPeople();
    props.getCountries();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...movieEntity,
        ...values,
        genres: mapIdList(values.genres),
        actors: mapIdList(values.actors),
        directors: mapIdList(values.directors)
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ratingApp.movie.home.createOrEditLabel">Create or edit a Movie</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : movieEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="movie-id">ID</Label>
                  <AvInput id="movie-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="titleLabel" for="movie-title">
                  Title
                </Label>
                <AvField
                  id="movie-title"
                  type="text"
                  name="title"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="releaseLabel" for="movie-release">
                  Release
                </Label>
                <AvField
                  id="movie-release"
                  type="string"
                  className="form-control"
                  name="release"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="durationLabel" for="movie-duration">
                  Duration
                </Label>
                <AvField
                  id="movie-duration"
                  type="string"
                  className="form-control"
                  name="duration"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="movie-genre">Genre</Label>
                <AvInput
                  id="movie-genre"
                  type="select"
                  multiple
                  className="form-control"
                  name="genres"
                  value={movieEntity.genres && movieEntity.genres.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {genres
                    ? genres.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="movie-actor">Actor</Label>
                <AvInput
                  id="movie-actor"
                  type="select"
                  multiple
                  className="form-control"
                  name="actors"
                  value={movieEntity.actors && movieEntity.actors.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {people
                    ? people.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="movie-director">Director</Label>
                <AvInput
                  id="movie-director"
                  type="select"
                  multiple
                  className="form-control"
                  name="directors"
                  value={movieEntity.directors && movieEntity.directors.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {people
                    ? people.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="movie-country">Country</Label>
                <AvInput id="movie-country" type="select" className="form-control" name="countryId">
                  <option value="" key="0" />
                  {countries
                    ? countries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/movie" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  genres: storeState.genre.entities,
  people: storeState.person.entities,
  countries: storeState.country.entities,
  movieEntity: storeState.movie.entity,
  loading: storeState.movie.loading,
  updating: storeState.movie.updating,
  updateSuccess: storeState.movie.updateSuccess
});

const mapDispatchToProps = {
  getGenres,
  getPeople,
  getCountries,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MovieUpdate);
