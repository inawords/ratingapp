import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './movie.reducer';
import { IMovie } from 'app/shared/model/movie.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMovieDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MovieDetail = (props: IMovieDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { movieEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Movie [<b>{movieEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="title">Title</span>
          </dt>
          <dd>{movieEntity.title}</dd>
          <dt>
            <span id="release">Release</span>
          </dt>
          <dd>{movieEntity.release}</dd>
          <dt>
            <span id="duration">Duration</span>
          </dt>
          <dd>{movieEntity.duration}</dd>
          <dt>Genre</dt>
          <dd>
            {movieEntity.genres
              ? movieEntity.genres.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === movieEntity.genres.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>Actor</dt>
          <dd>
            {movieEntity.actors
              ? movieEntity.actors.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === movieEntity.actors.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>Director</dt>
          <dd>
            {movieEntity.directors
              ? movieEntity.directors.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === movieEntity.directors.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>Country</dt>
          <dd>{movieEntity.countryId ? movieEntity.countryId : ''}</dd>
        </dl>
        <Button tag={Link} to="/movie" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/movie/${movieEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ movie }: IRootState) => ({
  movieEntity: movie.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);
