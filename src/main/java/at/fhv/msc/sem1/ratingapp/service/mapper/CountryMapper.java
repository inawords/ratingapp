package at.fhv.msc.sem1.ratingapp.service.mapper;

import at.fhv.msc.sem1.ratingapp.domain.*;
import at.fhv.msc.sem1.ratingapp.service.dto.CountryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Country} and its DTO {@link CountryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CountryMapper extends EntityMapper<CountryDTO, Country> {


    @Mapping(target = "movies", ignore = true)
    @Mapping(target = "removeMovie", ignore = true)
    @Mapping(target = "people", ignore = true)
    @Mapping(target = "removePerson", ignore = true)
    Country toEntity(CountryDTO countryDTO);

    default Country fromId(Long id) {
        if (id == null) {
            return null;
        }
        Country country = new Country();
        country.setId(id);
        return country;
    }
}
