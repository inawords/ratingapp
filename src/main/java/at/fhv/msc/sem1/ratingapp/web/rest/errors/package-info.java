/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package at.fhv.msc.sem1.ratingapp.web.rest.errors;
