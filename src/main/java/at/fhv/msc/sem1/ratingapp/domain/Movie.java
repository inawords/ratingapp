package at.fhv.msc.sem1.ratingapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Movie.
 */
@Entity
@Table(name = "movie")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "release", nullable = false)
    private Integer release;

    @NotNull
    @Column(name = "duration", nullable = false)
    private Integer duration;

    @OneToMany(mappedBy = "movie")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Rating> ratings = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "movie_genre",
               joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "id"))
    private Set<Genre> genres = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "movie_actor",
               joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "actor_id", referencedColumnName = "id"))
    private Set<Person> actors = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "movie_director",
               joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "director_id", referencedColumnName = "id"))
    private Set<Person> directors = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("movies")
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Movie title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRelease() {
        return release;
    }

    public Movie release(Integer release) {
        this.release = release;
        return this;
    }

    public void setRelease(Integer release) {
        this.release = release;
    }

    public Integer getDuration() {
        return duration;
    }

    public Movie duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public Movie ratings(Set<Rating> ratings) {
        this.ratings = ratings;
        return this;
    }

    public Movie addRating(Rating rating) {
        this.ratings.add(rating);
        rating.setMovie(this);
        return this;
    }

    public Movie removeRating(Rating rating) {
        this.ratings.remove(rating);
        rating.setMovie(null);
        return this;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public Movie genres(Set<Genre> genres) {
        this.genres = genres;
        return this;
    }

    public Movie addGenre(Genre genre) {
        this.genres.add(genre);
        genre.getMovies().add(this);
        return this;
    }

    public Movie removeGenre(Genre genre) {
        this.genres.remove(genre);
        genre.getMovies().remove(this);
        return this;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public Set<Person> getActors() {
        return actors;
    }

    public Movie actors(Set<Person> people) {
        this.actors = people;
        return this;
    }

    public Movie addActor(Person person) {
        this.actors.add(person);
        person.getStarrings().add(this);
        return this;
    }

    public Movie removeActor(Person person) {
        this.actors.remove(person);
        person.getStarrings().remove(this);
        return this;
    }

    public void setActors(Set<Person> people) {
        this.actors = people;
    }

    public Set<Person> getDirectors() {
        return directors;
    }

    public Movie directors(Set<Person> people) {
        this.directors = people;
        return this;
    }

    public Movie addDirector(Person person) {
        this.directors.add(person);
        person.getProductions().add(this);
        return this;
    }

    public Movie removeDirector(Person person) {
        this.directors.remove(person);
        person.getProductions().remove(this);
        return this;
    }

    public void setDirectors(Set<Person> people) {
        this.directors = people;
    }

    public Country getCountry() {
        return country;
    }

    public Movie country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Movie)) {
            return false;
        }
        return id != null && id.equals(((Movie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Movie{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", release=" + getRelease() +
            ", duration=" + getDuration() +
            "}";
    }
}
