package at.fhv.msc.sem1.ratingapp.service.impl;

import at.fhv.msc.sem1.ratingapp.service.CinemaService;
import at.fhv.msc.sem1.ratingapp.domain.Cinema;
import at.fhv.msc.sem1.ratingapp.repository.CinemaRepository;
import at.fhv.msc.sem1.ratingapp.service.dto.CinemaDTO;
import at.fhv.msc.sem1.ratingapp.service.mapper.CinemaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Cinema}.
 */
@Service
@Transactional
public class CinemaServiceImpl implements CinemaService {

    private final Logger log = LoggerFactory.getLogger(CinemaServiceImpl.class);

    private final CinemaRepository cinemaRepository;

    private final CinemaMapper cinemaMapper;

    public CinemaServiceImpl(CinemaRepository cinemaRepository, CinemaMapper cinemaMapper) {
        this.cinemaRepository = cinemaRepository;
        this.cinemaMapper = cinemaMapper;
    }

    /**
     * Save a cinema.
     *
     * @param cinemaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CinemaDTO save(CinemaDTO cinemaDTO) {
        log.debug("Request to save Cinema : {}", cinemaDTO);
        Cinema cinema = cinemaMapper.toEntity(cinemaDTO);
        cinema = cinemaRepository.save(cinema);
        return cinemaMapper.toDto(cinema);
    }

    /**
     * Get all the cinemas.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<CinemaDTO> findAll() {
        log.debug("Request to get all Cinemas");
        return cinemaRepository.findAll().stream()
            .map(cinemaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one cinema by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CinemaDTO> findOne(Long id) {
        log.debug("Request to get Cinema : {}", id);
        return cinemaRepository.findById(id)
            .map(cinemaMapper::toDto);
    }

    /**
     * Delete the cinema by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Cinema : {}", id);
        cinemaRepository.deleteById(id);
    }
}
