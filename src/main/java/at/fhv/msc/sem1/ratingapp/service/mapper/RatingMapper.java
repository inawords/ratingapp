package at.fhv.msc.sem1.ratingapp.service.mapper;

import at.fhv.msc.sem1.ratingapp.domain.*;
import at.fhv.msc.sem1.ratingapp.service.dto.RatingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Rating} and its DTO {@link RatingDTO}.
 */
@Mapper(componentModel = "spring", uses = {MovieMapper.class})
public interface RatingMapper extends EntityMapper<RatingDTO, Rating> {

    @Mapping(source = "movie.id", target = "movieId")
    RatingDTO toDto(Rating rating);

    @Mapping(source = "movieId", target = "movie")
    Rating toEntity(RatingDTO ratingDTO);

    default Rating fromId(Long id) {
        if (id == null) {
            return null;
        }
        Rating rating = new Rating();
        rating.setId(id);
        return rating;
    }
}
