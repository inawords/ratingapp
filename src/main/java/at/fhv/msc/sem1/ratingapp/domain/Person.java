package at.fhv.msc.sem1.ratingapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Person.
 */
@Entity
@Table(name = "person")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "is_actor")
    private Boolean isActor;

    @Column(name = "is_director")
    private Boolean isDirector;

    @ManyToOne
    @JsonIgnoreProperties("people")
    private Country country;

    @ManyToMany(mappedBy = "actors")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Movie> starrings = new HashSet<>();

    @ManyToMany(mappedBy = "directors")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Movie> productions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Person firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Person lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean isIsActor() {
        return isActor;
    }

    public Person isActor(Boolean isActor) {
        this.isActor = isActor;
        return this;
    }

    public void setIsActor(Boolean isActor) {
        this.isActor = isActor;
    }

    public Boolean isIsDirector() {
        return isDirector;
    }

    public Person isDirector(Boolean isDirector) {
        this.isDirector = isDirector;
        return this;
    }

    public void setIsDirector(Boolean isDirector) {
        this.isDirector = isDirector;
    }

    public Country getCountry() {
        return country;
    }

    public Person country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Movie> getStarrings() {
        return starrings;
    }

    public Person starrings(Set<Movie> movies) {
        this.starrings = movies;
        return this;
    }

    public Person addStarring(Movie movie) {
        this.starrings.add(movie);
        movie.getActors().add(this);
        return this;
    }

    public Person removeStarring(Movie movie) {
        this.starrings.remove(movie);
        movie.getActors().remove(this);
        return this;
    }

    public void setStarrings(Set<Movie> movies) {
        this.starrings = movies;
    }

    public Set<Movie> getProductions() {
        return productions;
    }

    public Person productions(Set<Movie> movies) {
        this.productions = movies;
        return this;
    }

    public Person addProduction(Movie movie) {
        this.productions.add(movie);
        movie.getDirectors().add(this);
        return this;
    }

    public Person removeProduction(Movie movie) {
        this.productions.remove(movie);
        movie.getDirectors().remove(this);
        return this;
    }

    public void setProductions(Set<Movie> movies) {
        this.productions = movies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        return id != null && id.equals(((Person) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Person{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", isActor='" + isIsActor() + "'" +
            ", isDirector='" + isIsDirector() + "'" +
            "}";
    }
}
