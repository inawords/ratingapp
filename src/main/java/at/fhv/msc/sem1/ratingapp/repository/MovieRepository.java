package at.fhv.msc.sem1.ratingapp.repository;

import at.fhv.msc.sem1.ratingapp.domain.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Movie entity.
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(value = "select distinct movie from Movie movie left join fetch movie.genres left join fetch movie.actors left join fetch movie.directors",
        countQuery = "select count(distinct movie) from Movie movie")
    Page<Movie> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct movie from Movie movie left join fetch movie.genres left join fetch movie.actors left join fetch movie.directors")
    List<Movie> findAllWithEagerRelationships();

    @Query("select movie from Movie movie left join fetch movie.genres left join fetch movie.actors left join fetch movie.directors where movie.id =:id")
    Optional<Movie> findOneWithEagerRelationships(@Param("id") Long id);

}
