package at.fhv.msc.sem1.ratingapp.web.rest;

import at.fhv.msc.sem1.ratingapp.service.CinemaService;
import at.fhv.msc.sem1.ratingapp.web.rest.errors.BadRequestAlertException;
import at.fhv.msc.sem1.ratingapp.service.dto.CinemaDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.fhv.msc.sem1.ratingapp.domain.Cinema}.
 */
@RestController
@RequestMapping("/api")
public class CinemaResource {

    private final Logger log = LoggerFactory.getLogger(CinemaResource.class);

    private static final String ENTITY_NAME = "cinema";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CinemaService cinemaService;

    public CinemaResource(CinemaService cinemaService) {
        this.cinemaService = cinemaService;
    }

    /**
     * {@code POST  /cinemas} : Create a new cinema.
     *
     * @param cinemaDTO the cinemaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cinemaDTO, or with status {@code 400 (Bad Request)} if the cinema has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cinemas")
    public ResponseEntity<CinemaDTO> createCinema(@Valid @RequestBody CinemaDTO cinemaDTO) throws URISyntaxException {
        log.debug("REST request to save Cinema : {}", cinemaDTO);
        if (cinemaDTO.getId() != null) {
            throw new BadRequestAlertException("A new cinema cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CinemaDTO result = cinemaService.save(cinemaDTO);
        return ResponseEntity.created(new URI("/api/cinemas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cinemas} : Updates an existing cinema.
     *
     * @param cinemaDTO the cinemaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cinemaDTO,
     * or with status {@code 400 (Bad Request)} if the cinemaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cinemaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cinemas")
    public ResponseEntity<CinemaDTO> updateCinema(@Valid @RequestBody CinemaDTO cinemaDTO) throws URISyntaxException {
        log.debug("REST request to update Cinema : {}", cinemaDTO);
        if (cinemaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CinemaDTO result = cinemaService.save(cinemaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cinemaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cinemas} : get all the cinemas.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cinemas in body.
     */
    @GetMapping("/cinemas")
    public List<CinemaDTO> getAllCinemas() {
        log.debug("REST request to get all Cinemas");
        return cinemaService.findAll();
    }

    /**
     * {@code GET  /cinemas/:id} : get the "id" cinema.
     *
     * @param id the id of the cinemaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cinemaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cinemas/{id}")
    public ResponseEntity<CinemaDTO> getCinema(@PathVariable Long id) {
        log.debug("REST request to get Cinema : {}", id);
        Optional<CinemaDTO> cinemaDTO = cinemaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cinemaDTO);
    }

    /**
     * {@code DELETE  /cinemas/:id} : delete the "id" cinema.
     *
     * @param id the id of the cinemaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cinemas/{id}")
    public ResponseEntity<Void> deleteCinema(@PathVariable Long id) {
        log.debug("REST request to delete Cinema : {}", id);
        cinemaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
