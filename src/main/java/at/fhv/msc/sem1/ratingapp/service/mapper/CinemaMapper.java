package at.fhv.msc.sem1.ratingapp.service.mapper;

import at.fhv.msc.sem1.ratingapp.domain.*;
import at.fhv.msc.sem1.ratingapp.service.dto.CinemaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cinema} and its DTO {@link CinemaDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CinemaMapper extends EntityMapper<CinemaDTO, Cinema> {



    default Cinema fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cinema cinema = new Cinema();
        cinema.setId(id);
        return cinema;
    }
}
