package at.fhv.msc.sem1.ratingapp.repository;

import at.fhv.msc.sem1.ratingapp.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
