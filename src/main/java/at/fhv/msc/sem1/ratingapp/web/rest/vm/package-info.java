/**
 * View Models used by Spring MVC REST controllers.
 */
package at.fhv.msc.sem1.ratingapp.web.rest.vm;
