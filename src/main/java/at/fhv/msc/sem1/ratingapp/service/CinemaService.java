package at.fhv.msc.sem1.ratingapp.service;

import at.fhv.msc.sem1.ratingapp.service.dto.CinemaDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link at.fhv.msc.sem1.ratingapp.domain.Cinema}.
 */
public interface CinemaService {

    /**
     * Save a cinema.
     *
     * @param cinemaDTO the entity to save.
     * @return the persisted entity.
     */
    CinemaDTO save(CinemaDTO cinemaDTO);

    /**
     * Get all the cinemas.
     *
     * @return the list of entities.
     */
    List<CinemaDTO> findAll();


    /**
     * Get the "id" cinema.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CinemaDTO> findOne(Long id);

    /**
     * Delete the "id" cinema.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
