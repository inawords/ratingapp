package at.fhv.msc.sem1.ratingapp.service.mapper;

import at.fhv.msc.sem1.ratingapp.domain.*;
import at.fhv.msc.sem1.ratingapp.service.dto.PersonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Person} and its DTO {@link PersonDTO}.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface PersonMapper extends EntityMapper<PersonDTO, Person> {

    @Mapping(source = "country.id", target = "countryId")
    PersonDTO toDto(Person person);

    @Mapping(source = "countryId", target = "country")
    @Mapping(target = "starrings", ignore = true)
    @Mapping(target = "removeStarring", ignore = true)
    @Mapping(target = "productions", ignore = true)
    @Mapping(target = "removeProduction", ignore = true)
    Person toEntity(PersonDTO personDTO);

    default Person fromId(Long id) {
        if (id == null) {
            return null;
        }
        Person person = new Person();
        person.setId(id);
        return person;
    }
}
