package at.fhv.msc.sem1.ratingapp.service.mapper;

import at.fhv.msc.sem1.ratingapp.domain.*;
import at.fhv.msc.sem1.ratingapp.service.dto.MovieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {GenreMapper.class, PersonMapper.class, CountryMapper.class})
public interface MovieMapper extends EntityMapper<MovieDTO, Movie> {

    @Mapping(source = "country.id", target = "countryId")
    MovieDTO toDto(Movie movie);

    @Mapping(target = "ratings", ignore = true)
    @Mapping(target = "removeRating", ignore = true)
    @Mapping(target = "removeGenre", ignore = true)
    @Mapping(target = "removeActor", ignore = true)
    @Mapping(target = "removeDirector", ignore = true)
    @Mapping(source = "countryId", target = "country")
    Movie toEntity(MovieDTO movieDTO);

    default Movie fromId(Long id) {
        if (id == null) {
            return null;
        }
        Movie movie = new Movie();
        movie.setId(id);
        return movie;
    }
}
