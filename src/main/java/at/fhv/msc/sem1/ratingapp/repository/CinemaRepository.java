package at.fhv.msc.sem1.ratingapp.repository;

import at.fhv.msc.sem1.ratingapp.domain.Cinema;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Cinema entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CinemaRepository extends JpaRepository<Cinema, Long> {

}
