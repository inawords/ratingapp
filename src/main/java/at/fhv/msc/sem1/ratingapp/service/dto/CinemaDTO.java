package at.fhv.msc.sem1.ratingapp.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link at.fhv.msc.sem1.ratingapp.domain.Cinema} entity.
 */
public class CinemaDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer rooms;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CinemaDTO cinemaDTO = (CinemaDTO) o;
        if (cinemaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cinemaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CinemaDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rooms=" + getRooms() +
            "}";
    }
}
