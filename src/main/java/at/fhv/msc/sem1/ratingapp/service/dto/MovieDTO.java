package at.fhv.msc.sem1.ratingapp.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link at.fhv.msc.sem1.ratingapp.domain.Movie} entity.
 */
public class MovieDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    @NotNull
    private Integer release;

    @NotNull
    private Integer duration;


    private Set<GenreDTO> genres = new HashSet<>();

    private Set<PersonDTO> actors = new HashSet<>();

    private Set<PersonDTO> directors = new HashSet<>();

    private Long countryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRelease() {
        return release;
    }

    public void setRelease(Integer release) {
        this.release = release;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Set<GenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(Set<GenreDTO> genres) {
        this.genres = genres;
    }

    public Set<PersonDTO> getActors() {
        return actors;
    }

    public void setActors(Set<PersonDTO> people) {
        this.actors = people;
    }

    public Set<PersonDTO> getDirectors() {
        return directors;
    }

    public void setDirectors(Set<PersonDTO> people) {
        this.directors = people;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MovieDTO movieDTO = (MovieDTO) o;
        if (movieDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), movieDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", release=" + getRelease() +
            ", duration=" + getDuration() +
            ", countryId=" + getCountryId() +
            "}";
    }
}
